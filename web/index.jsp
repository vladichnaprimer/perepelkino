<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta name="google-site-verification" content="QbpWDEfcjVPel7Pde-qQoeSVdFdDyWMOVU-ldXXAuuI" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author" content="Vladich">
  <meta name="keywords" content="перепелкино, перепела, перелепелиные яйца, тушки перепела, мясо, ферма, купить, куплю, лоток,
                                 домашние перепелиные яйца, перепелиные яйца украина, перепелиные яйца цены, перепелиные яйца харьков, харьковская область,
                                 тушка, цыплята, куры-несушки,
                                 инкубационное яйцо, Фараон, Маньчжурский, Техасский альбинос" >
    <link rel="shortcut icon" href="img/perepell.png">

  <title>Домашние перепелиные яйца купить в Харькове</title>

  <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
  <script type="text/javascript" src="js/jssor.slider.mini.js"></script>

  <!-- Custom CSS -->
  <link href="css/freelancer.css" rel="stylesheet">


  <!-- Custom Fonts -->
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">



   <script>
        jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
                {$Duration:1200,x:-0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
                {$Duration:1200,x:0.3,$SlideOut:true,$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
                $AutoPlay: true,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $Cols: 1,
                    $Align: 0
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1000);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
   </script>
   <style>
       /* jssor slider bullet navigator skin 01 css */
       /*
       .jssorb01 div           (normal)
       .jssorb01 div:hover     (normal mouseover)
       .jssorb01 .av           (active)
       .jssorb01 .av:hover     (active mouseover)
       .jssorb01 .dn           (mousedown)
       */
       .jssorb01 {
           position: absolute;
       }
       .jssorb01 div, .jssorb01 div:hover, .jssorb01 .av {
           position: absolute;
           /* size of bullet elment */
           width: 8px;
           height: 8px;
           filter: alpha(opacity=70);
           opacity: .7;
           overflow: hidden;
           cursor: pointer;
           border: #000 1px solid;
       }
       .jssorb01 div { background-color: gray; }
       .jssorb01 div:hover, .jssorb01 .av:hover { background-color: #d3d3d3; }
       .jssorb01 .av { background-color: #fff; }
       .jssorb01 .dn, .jssorb01 .dn:hover { background-color: #555555; }

       /* jssor slider arrow navigator skin 05 css */
       /*
       .jssora05l                  (normal)
       .jssora05r                  (normal)
       .jssora05l:hover            (normal mouseover)
       .jssora05r:hover            (normal mouseover)
       .jssora05l.jssora05ldn      (mousedown)
       .jssora05r.jssora05rdn      (mousedown)
       */
       .jssora05l, .jssora05r {
           display: block;
           position: absolute;
           /* size of arrow element */
           width: 50px;
           height: 50px;
           cursor: pointer;
           background: url('img/a17.png') no-repeat;
           overflow: hidden;

       }
       .jssora05l { background-position: -10px -40px; }
       .jssora05r { background-position: -70px -40px; }
       .jssora05l:hover { background-position: -130px -40px; }
       .jssora05r:hover { background-position: -190px -40px; }
       .jssora05l.jssora05ldn { background-position: -250px -40px; }
       .jssora05r.jssora05rdn { background-position: -310px -40px; }

       /* jssor slider thumbnail navigator skin 09 css */

       .jssort09-600-45 .p {
           position: absolute;
           top: 0;
           left: 0;
           width: 600px;
           height: 45px;
       }

       .jssort09-600-45 .t {
           font-family: verdana;
           font-weight: normal;
           position: absolute;
           width: 100%;
           height: 100%;
           top: 0;
           left: 0;
           color:#fff;
           line-height: 45px;
           font-size: 20px;
           padding-left: 10px;
       }
   </style>


    <%--Скрипт для отслеживания сайта--%>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-73416964-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body id="page-top" class="index">



<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
      <a class="navbar-brand" href="#page-top">PEREPЁLKINO</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="navbar-collapse " >
      <ul class="nav navbar-nav navbar-right">
        <li class="page-scroll">
          <a href="#about">О нас</a>
        </li>
        <li class="page-scroll">
          <a href="#buy">Продукция</a>
        </li>
       <%-- <li class="page-scroll">
              <a href="#feedback">Обратная связь</a>
        </li>--%>
          <li class="page-scroll">
              <a href="#gallery">Галерея</a>
          </li>
        <li class="page-scroll">
          <a href="#contacts">Контакты</a>
        </li>
      </ul>
    </div>
    <!--  /.navbar-collapse-->
  </div>
  <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header>
  <div class="container">
      <br/><br/><br/>
    <div class="row" style="background-color: #128f76">
      <div class="col-lg-12">
        <img class="img-responsive" src="img/perepell.png" alt="">
        <div class="intro-text" >
          <span class="name">PEREPЁLKINO</span>
          <hr class=star-primary>
          <span class="skills"></span>
        </div>
      </div>
    </div> <br/><br/><br/><br/>
  </div>
</header>

<!-- About Section -->
<section class="success" id="about" style="background-color: #597ea2">
    <br/><br/><br/>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>О нас</h2>
        <hr class="star-primary">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 ">                                                              <!-- ГОТОВО -->
        <h6 style="font-size: 28px">Perepelkino  - эко-ферма, занимающиеся производством перепелиной продукции.
          Мы успешно работаем на протяжении 5 лет и наши клиенты всегда довольны продуктом.</h6>
      </div>
      <div class="col-lg-6">
        <h6 style="font-size: 28px">В процессе выращивания перепелов мы не используюем химические добавки. Корм сугубо натурального происхождения, поэтому
        продукт <b>гарантировано</b> экологически чистый.</h6>
      </div>

    </div>
    <br/><br/><br/><br/><br/><br/><br/><br/>
  </div>
</section>

<!-- Production Section -->
<section id="buy" style="background-color: #8aa4be">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>Наша продукция </h2>
        <hr class="star-primary">
        </div>
        <div class="row">

          <div class = "text-center">


              <div  class="footer-col col-md-6" style="background-color:#8aa4be">
               <h3>Перепелиное яйцо  </h3>
                  <h4>Лоток - 18 грн</h4>
                  <img  src="img/eggs.jpg" alt="Перепелиные яйца">
                  <br/><br/><br/>
              </div>
              <br/><br/>
              <div class="footer-col col-md-6" style="background-color:#8aa4be ">
                <h3> Перепелиное мясо</h3>
                <h4>85 грн/кг</h4>
                <img src="img/2.PNG" alt="перепелиные тушки">
                  <br/><br/><br/>
              </div>
              <br/><br/>
              <div class="footer-col col-md-6" style="background-color:#8aa4be ">
                  <h3> Цыплята/куры-несушки</h3>
                  <h4>Уточняйте цену</h4>
                  <img  src="img/mini.png" alt="цыплята">
                  <br/><br/><br/>
              </div>
              <div class="footer-col col-md-6" style="background-color:#8aa4be ">
                  <h3> Инкубационное яйцо</h3>
                  <br/><br/><br/>
                  <table style="border: none, ">
                      <tr>
                          <h3 style="font-size: 32px">Фараон - 1,5 грн</h3>
                          <h3 style="font-size: 32px">Маньчжурский - 1,5 грн</h3>
                          <h3 style="font-size: 32px">Техасский альбинос - 2,5 грн</h3>
                      </tr>
                  </table>
                  <br/><br/><br/><br/><br/>
              </div>
          </div>

    </div>
    <br/><br/><br/><br/><br/>
  </div>
  </div>
</section>

<section id="gallery" style="background-color:#128f76">
    <br/>
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Галерея </h2>
            <hr class="star-primary">
        </div>
    </div>
    <br/><br/>
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden;">
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/mini.png" />
                <div data-u="thumb">Цыпленок Смокинговый</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird8.JPG" />
                <div data-u="thumb">Цыпленок Фараона</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird9.JPG" />
                <div data-u="thumb">Маньчжурский золотистый</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird91.JPG" />
                <div data-u="thumb">Молодой техасец</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird1.JPG" />
                <div data-u="thumb">Московский великан</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird2.JPG" />
                <div data-u="thumb"></div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird3.JPG" />
                <div data-u="thumb">Техасец</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird4.JPG" />
                <div data-u="thumb"></div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird5.JPG" />
                <div data-u="thumb">Фараон и Техасец</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird6.JPG" />
                <div data-u="thumb"></div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird7.JPG" />
                <div data-u="thumb">Фараон и Маньчжурец</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird92.JPG" />
                <div data-u="thumb">Брудер</div>
            </div>
            <div data-p="112.50" style="display: none;">
                <img data-u="image" src="img/bird93.JPG" />
                <div data-u="thumb">Курятник</div>
            </div>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort09-600-45" style="position:absolute;bottom:0px;left:0px;width:600px;height:45px;">
            <div style="position: absolute; top: 0; left: 0; width: 100%; height:100%; background-color: #000; filter:alpha(opacity=40.0); opacity:0.4;"></div>
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div data-u="thumbnailtemplate" class="t"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb01" style="bottom:16px;right:16px;">
            <div data-u="prototype" style="width:12px;height:12px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:0px;left:8px;width:40px;height:40px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
        <a href="http://www.jssor.com" style="display:none">Slideshow Maker</a>
    </div>
    <br/><br/><br/><br/><br/><br/>

</section>

<%--<section id="feedback" style="background-color:#128f76">
    <div class="container" style="text-align: left; /*margin-top: 10px;*/">
        <h6 style="font-size: 28px; color: #ffffff">Вы всегда можете задать нам вопрос, написав на почту: </h6>
    </div>
    <div class="container" style="text-align: center; /*margin-top: 10px;*/">
        <a href="#contact" data-toggle="modal" class="btn btn-primary btn-lg">Обратная связь</a>
    </div>
    <div id="contact" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="contact-form" method="post" action="${pageContext.request.contextPath}/mail" class="modal-body" role="form">
                    <div class="form-group">
                        <input class="form-control input-lg" type="text" placeholder="Ваше имя" required name="subject">
                    </div>
                    <div class="form-group">
                        <input class="form-control input-lg" type="email" placeholder="Ваш email" required name="from">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control input-lg" rows="5" placeholder="Ваше сообщение" required name="message" ></textarea>
                    </div>
                </form>
                <div class="modal-footer">
                    <button class="btn btn-default btn-lg" type="submit" title="Отправить Email" form="contact-form">Отправить</button>
                </div>
            </div>
        </div>
    </div>

</section>--%>



<!-- Footer -->
<footer class="text-center" id="contacts">
  <div class="footer-above">
    <div class="container">
      <div class="row">
        <div class="footer-col col-md-4">
          <h3>Контакты</h3>
          <p>Виктор <br/>
              +380679004147 <br/>
              +380503032648 <br/>
          </p>
        </div>
        <div class="footer-col col-md-4">
          <h3>Местоположение</h3>
          <p>Украина, г. Харьков<br/>
            м. Проспект Гагарина <br/>
          </p>
        </div>
        <div class="footer-col col-md-4">
          <h3>Мы в сети</h3>
          <ul class="list-inline">
            <li>
              <a href="https://plus.google.com/u/4/117392022635909721315/posts" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="center">
            <h3>Как к нам доехать</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2195.5470330993767!2d36.235165810711415!3d49.97188072849941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1455380293336"
                    width="700" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
  <div class="footer-below">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          Copyright &copy; Perepelkino 2015
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visible-sm">
  <a class="btn btn-primary" href="#page-top">
    <i class="fa fa-chevron-up"></i>
  </a>
</div>


<%--Ajax script
<script>
function doGet(e) {
    try {
        MailApp.sendEmail('perepelkino12@gmail.com', 'Feed from perepelkinofarm-eggsua.rhcloud.com', e.parameters.message, {
            htmlBody: '<p>from: ' + e.parameters.name + '</p>' + '<p>email: ' + e.parameters.mail + '</p>' +
                    '<p>message: ' + e.parameters.message + '</p>'
        });
        return ContentService.createTextOutput(e.parameters.cb + '("OK")').setMimeType(ContentService.MimeType.JAVASCRIPT);
    } catch(e) {
        return ContentService.createTextOutput(e.parameters.cb + '("Error")').setMimeType(ContentService.MimeType.JAVASCRIPT);
    }
}
</script>

<script>
    $('#contact-form').on('submit', function(e) {
        e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
        var that = $(e.target); // получаем ссылку на источник события - форму #contact-form
        $.ajax({ // отправляем данные
            // URL развернутого скрипта Google Apps Script
            url: 'https://script.google.com/macros/s/AKfycby2HeahiWEdoKK0ZwXAPk5xjivrg-Yrs3qZu8hwxx5lSOhCaXU/exec',
            data: $(this).serialize(), // собираем запрос
            jsonp: 'cb', // имя параметра запроса
            jsonpCallback: 'bingo', // имя функции
            dataType:'jsonp', // тип данных
            success: function bingo(data){
                console.log(data); // проверим данные, полученные с бэкэнда
                if (data == 'Error') {
                    alertForm({form: that, type: 'alert-danger', msg: 'Не удалось отправить сообщение.'});
                    return;
                }
                alertForm({form: that, type: 'alert-success', msg: 'Ваше сообщение отправлено.'});
                that.find('textarea').val('');
            },
            error: function(){
                alertForm({form: that, type: 'alert-danger', msg: 'Не удалось отправить сообщение.'});
            }
        });
    });
    // функция вывода сообщений в модальную форму
    function alertForm(alert) {
        var div = $('<div class="alert ' + alert.type + '" style="display: none;">' + alert.msg + '</div>');
        alert.form.prepend(div);
        div.slideDown(400).delay(3000).slideUp(400, function() {
            alert.form.closest('.modal').modal('hide');
            div.remove();
        });
    }
</script>--%>
<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="js/freelancer.js"></script>


</body>

</html>