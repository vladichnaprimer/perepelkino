package com.perepelkino.controller;

import com.perepelkino.util.MainSender;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MailServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        new MainSender.MainSenderBuilder()
                .setFrom(req.getParameter("from"))
                .setSubject(req.getParameter("subject"))
                .setText(req.getParameter("message"))
                .build();
        resp.sendRedirect(req.getHeader("referer"));

    }
}
