package com.perepelkino.util;

import javax.mail.*;
import javax.mail.internet.*;

import java.util.Properties;

public final class MainSender {

    private final String to = "perepelkino12@gmail.com";
    private final String password = "perepelkino8";
    private String from;
    private String text;
    private String subject;

    public MainSender(MainSenderBuilder mainSenderBuilder) {
        this.from = mainSenderBuilder.from;
        this.text = mainSenderBuilder.text;
        this.subject = mainSenderBuilder.subject;
    }

    public static class MainSenderBuilder{
        private String from;
        private String text;
        private String subject;

        public MainSenderBuilder setFrom(String from) {
            this.from = from;
            return this;
        }

        public MainSenderBuilder setText(String text) {
            this.text = text;
            return this;
        }

        public MainSenderBuilder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public MainSender build(){
            MainSender mainSender = new MainSender(this);
            mainSender.sendMain();
            return mainSender;
        }
    }


    private void sendMain(){
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(to, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("Testing Subject");
            message.setText("Dear Mail Crawler,"
                    + "\n\n No spam to my email, please!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
